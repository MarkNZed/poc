### Overview ###
This is a Proof of Concept (PoC) for the PropaCov framework. PropaCov is an application being developed for the microelectronics industry. The idea for this PoC is to explore a generic Python framework and in a later step use this framework for PropaCov.

Instead of running PropaCov this PoC runs a program that plays chess. The current versions is a first exploration of the PoC. It shows a data flow from browser -> web browser -> job queue -> worker -> messages -> web server -> browser. In a future version we intend to have workers play out a game of chess, this will be a placeholder for PropaCov running.

See architecture_sketch.png for an overview of where we are heading. Currently there are two processes: flaskapp.py & workercli.py

### Setup ###

 * Install redis (not necessary if you are using our Docker environment)
 * Install virtualenv e.g `apt-get install virtualenv`
 * Create a virtualenv `virtualenv -p python3 venv`
 * Activate virtualenv `source venv/bin/activate`
 * Install python packages `pip install -r requirements.txt`

To run this in the docker container we need to setup the redis URL in the shell: `export REDIS_URL='redis://propacov_redis:6379'`

requirements.txt was generated using `pipreqs .`

### Running the chess (via Sunfish) example ###
```
./flaskapp.py 
WARNING:werkzeug: * Debugger is active!
```
Werkzeug is WSGI library that Flask microframework is based on.
Seeing this message means that debugging is active: you'll get formatted exceptions in your browser, and other debugging features.
Debug setting can be changed usually in the last line of the main flask file, at deployment; debugging and reloading (getting py files changes from filesystem) can be on and off independently:
```
app.run(debug=True, host="0.0.0.0", port=5003, use_reloader=True, threaded=True)
```
For the worker, *.py changes reloaded if library pre-loading is not done in /workercli.py (as each job is forked, and has therefore new copy of the libraries): so in development, both flask and worker processes can reload changes that you make in your editor/IDE.
Worker for the default queue can be run like this:
```
./workercli.py
15:12:22 default: common.tasks.make_move('e2e4', '') (fb38440c-9e19-422e-a942-4adc9426c1c3)
15:12:22 default: Job OK (fb38440c-9e19-422e-a942-4adc9426c1c3)
15:12:22 Result is kept for 500 seconds
```
(the job will appear after submitting a form, on which see later)

"Result is kept for 500 seconds" is the Python-RQ module message, it means by default whatever is returned by task function will live for 500s in the Redis. Our worker doesn't return results, it submits a message onto another queue, so code that initiates job (in Flask) doesn't wait for result via Python-RQ.

### What it does on a high level ###
The text input field accepts a command to make a move, like
```
e2e4
```
famous opening move. Then Flask posts a job, via Redis, for a worker to run position through Sunfish mini-engine, which is given 3 seconds to generate the next move. The next move and new position are returned via PubSub channel, also in Redis. Flask app listens to that, and formats the output (in /webapp/sse.py) for the browser, using Server Side Event (SSE) protocol.
The board is stored in an input of type "hidden" in the web form, and passed back to Flask and then to the worker.
Actually it would need to serialise entire Position class of Sunfish for the game to run correctly; this is not done in this example.

### More low-level details ###
For testing this example, you need a browser that supports both JS and SSE — as there's no polyfill for older browsers for:
```
var source = new EventSource('/stream');
```
The board is passed as 120 chars (with padding) from worker to webapp in these lines of common/tasks.py:
```
    config.redis_progress.publish(config.progress_channel, "%s\n%s" % (render(119-move[0]) + render(119-move[1]), pos.board))
    config.redis_progress.publish(config.progress_channel, "%s" % pos.board)
```
Flask delivers them over SSE in flaskapp.py lines 22-34: there's a generator func and /stream endpoint that waits on it.
Lines 60-67 of flaskapp hardcode JS within HTML that receives those two messages.
The rest of flaskapp is sending new move to the worker(s).

Whoever is waiting on /stream URL (it could be more than one client) receive the notification. Right now it's only one game that can be held and displayed to everyone. Having > 1 would require sending only to select subscribers (there are varius ways to implement that).

### Configuration ###
Config file is kept in /common/config.py and included in /common/tasks.py, /flaskapp.py and /workercli.py (it's quite customary for Python systems to have just *.py config(s), no yaml or other files — as no explicit compilation stage is needed to reconfigure the software). Of course, if config file needs to be written when using software, there are alternatives: Pickle for GUI-only config (Pickle is Python-only object serialisation format) and usual config files, JSON or YAML (e.g. Google App Engine Python env uses .yaml)

Currently /common/config.py contains two default values for Redis URLs.
### Notes ###
From ipython `!vi` to get to an editor
To start all the processes:
./run.sh
To start the Command Line Interface:
./bin/cli.py
From the CLI to start the game:
start()
### Redis Dashboard ###
Visible at http://XXX/rq
### PyLint ###
pip install pylint
pylint state.py
### Transactions ###
The workers can update the DB and the files. For the DB we use Two Phase commit, the commit is finalised by the State Server. This ensures that the State Server does not get out of sync with the DB. Jobs producing files should be idempotent so that a job run more than once does not corrupt the data.
The State Server's state is persisted to the DB so it can be interrupted and restart if the State Machine supports recovery.
