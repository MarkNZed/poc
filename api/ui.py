from common import config, const
from api import message

def start():
    content = message.Content('start')
    message.send(content, config.channel_ui)

def stop():
    content = message.Content('stop')
    message.send(content, config.channel_ui)

def toggle_pause():
    content = message.Content('toggle_pause')
    message.send(content, config.channel_ui)

def shutdown():
    content = message.Content('shutdown')
    message.send(content, config.channel_ui)

def restart():
    content = message.Content('restart')
    message.send(content, config.channel_ui)

def get_state():
    content = message.Content('get_state')
    message_id = message.send(content, config.channel_ui)
    result = message.wait_for(config.channel_state, message_id)
    return result.content.data['state']

def monitor_game():
	for m in message.listen(config.channel_game):
   		print(m.content)

def result_annotation(result):
	if result == const.RESULT_MAX_TURNS:
		return const.MAX_MOVE_MSG
	if result == const.RESULT_ABANDON:
		return "Abandoned"
	if result == const.RESULT_DRAW:
		return "Draw"
	if result == const.RESULT_STALEMATE:
		return "Stalemate"
	if result == const.RESULT_BLACK:
		return "Checkmate"
	if result == const.RESULT_WHITE:
		return "Checkmate"
	return "Not Defined Yet"