from common import config
from rq import Queue, get_failed_queue, Connection

queue_default = Queue('default', connection=config.redis_rq)

# TODO route exceptions differently? (default is 'failed' queue)
# http://python-rq.org/docs/exceptions/
# http://stackoverflow.com/questions/21270783/redis-queue-python-rq-right-pattern-to-prevent-high-memory-usage
def enqueue(state, *args):
	# We do not delete results, this is the job of the state server
	#return queue_default.enqueue(*args, result_ttl=-1)
	job = queue_default.enqueue(*args, result_ttl=0)
	# Treat jobs as a FIFO
	state.jobs.insert(0,job.id)
	return job

def delete_failed_jobs():
	with Connection(config.redis_rq):
		fq = get_failed_queue()
		while True:
			job = fq.dequeue()
			if not job:
				break
			job.delete()  # Will delete key from Redis

def failed_jobs_exist():
	with Connection(config.redis_rq):
		fq = get_failed_queue()
		if len(fq.jobs) > 0:
			return True
	return False
