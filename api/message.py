import datetime, pickle, inspect, os, sys
import logging

import logging
from common import config, utils
from pprint import pformat

logger = logging.getLogger('message')
logger.setLevel(logging.INFO)

redis_db = config.redis_messaging
message_num = 0

# Content object
# cmd is assumed to be a string
# param is assumed to be a dictionary
class Content:
	def __init__(self, cmd=None, data=None):
		self.cmd = cmd
		self.data = data

	def __repr__(self):
		return "<" + type(self).__name__ + ">\n" + pformat(vars(self), indent=4, width=1)

class Message:
	def __init__(self, src, channel, content):
		global message_num
		pid = os.getpid()
		message_num += 1
		self.src = src
		self.channel = channel
		self.content = content
		self.id = str(pid) + str(message_num)

	def cmd(self, channel, cmd):
		if self.channel == channel:
			if isinstance(cmd, list):
				for c in cmd:
					if self.content.cmd == c:
						return True
			else:
				if self.content.cmd == cmd:
					return True
			return False

	def __repr__(self):
		return "<" + type(self).__name__ + ">\n" + pformat(vars(self), indent=4, width=1)

# Required because Python 3.5 changes the structure of Frame object
def inspect_frame(frame):
	if sys.version_info >= (3, 5):
		return frame.filename, frame.lineno
	else:
		return frame[1], frame[2]

def build_message(content, channel, src=None):
	if src == None:
		filename, lineno = inspect_frame(inspect.stack()[2])
		src = "{} {}".format(filename, lineno)
	return Message(src, channel, content)

def send(content, channel, src=None):
	m = build_message(content, channel, src)
	logger.debug(m)
	redis_db.publish(channel, pickle.dumps(m))
	return m.id 

def listen(channel, init_channel=None):
	pubsub = redis_db.pubsub()
	if isinstance(channel, list):
		for ch in channel:
			pubsub.subscribe(ch)
	else:
		pubsub.subscribe(channel)
	if init_channel:
		content = Content('init')
		yield build_message(content, init_channel)
	for m in pubsub.listen():
		if (m['type'] == "message"):
			our_message = pickle.loads(m['data'])
			yield our_message
 
def wait_for(channel, id):
	pubsub = redis_db.pubsub()
	if isinstance(channel, list):
		for ch in channel:
			pubsub.subscribe(ch)
	else:
		pubsub.subscribe(channel)
	for m in pubsub.listen():
		if (m['type'] == "message"):
			our_message = pickle.loads(m['data'])
			if our_message.content.cmd == 'response' and our_message.content.data['id'] == id:
				return our_message
