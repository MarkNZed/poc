import logging
import sqlalchemy
from common import config
from sqlalchemy.orm import sessionmaker
from pprint import pformat, pprint

logger = logging.getLogger('sqlalchemy.engine')
logger.setLevel(logging.INFO)

# We assume two phase commit, the worker will perform:
#	db.add(obj)
#	db.prepare()
#	id = db.transaction_id()
# Then return the id with the result for second phase commit by State Server

# Need to set max_prepared_transactions to use these, which requires a server restart

Session = sessionmaker(bind=config.ENGINE, twophase=True)
session = Session()

def add(*args):
	return session.add(*args)

def prepare(*args):
	return session.prepare(*args)

def transaction_id():
	# Find transaction id

	for k, v in session.transaction._connections.items():
		if isinstance(k, sqlalchemy.engine.base.Connection):
			return k._Connection__transaction.xid
	# Should error here ?
	return None

def cleanup_prepared():
    conn = config.ENGINE.connect()
    conn.connection.connection.set_isolation_level(config.ISOLATION_LEVEL_AUTOCOMMIT)
    trs = [t[0] for t in conn.execute('select gid from pg_prepared_xacts')]
    #conn.execute("rollback;")
    for t in trs:
        # Could not use conn.rollback_prepared because it starts a transaction
        conn.execute("ROLLBACK PREPARED '"+t+"'")
    conn.connection.connection.set_isolation_level(config.ISOLATION_LEVEL_READ_COMMITTED)
    conn.close()
