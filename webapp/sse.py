import logging

from common import models
from common.const import RESULT_AND_ANNOTATION, STATE_RESULT, STATE_MOVE, STATE_PAUSED
from api import ui

logger = logging.getLogger(__name__)

# SSE "protocol" is described here: http://mzl.la/UPFyxY
class ServerSentEvent():

    def __init__(self):
        pass

    def post_init(self):
        self.desc_map = {
            self.data : "data",
            self.event : "event",
            self.id : "id"
        }

    def encode(self):
        if not self.data:
            return ""
        
        lines = [u"%s: %s" % (v, k) for k, v in self.desc_map.items() if k]
        
        return u"%s\n\n" % u"\n".join(lines)

class GameBoardEvent(ServerSentEvent):

    def __init__(self, content, db):
        super().__init__()
        self.event = None
        self.data = None
        datain = content.data
        db_move_id = datain.attr["db_move_id"]
        if db_move_id and (datain.name in [STATE_MOVE, STATE_RESULT] or (content.cmd == 'repeat' and datain.name == STATE_PAUSED)):
            self.event = "board_update"
            game_move = db.session.query(models.ChessGameMove).get(db_move_id)
            self.data = "{} {} {} {}".format(datain.attr["game_id"], game_move.turn, game_move.san, game_move.fen)
        logger.info(self.data)
        self.id = None
        self.post_init()

class StateServerEvent(ServerSentEvent):

    def __init__(self, content):
        super().__init__()
        self.event = None
        self.data = None
        datain = content.data
        if isinstance(datain, models.State):
            if datain.name == STATE_RESULT:
                result = datain.attr["game_result"]
                self.data = "result: {} {}".format(*RESULT_AND_ANNOTATION[result])
                self.event = "result_update"
            else:
                self.data = "state: {}".format(datain.name)
                self.event = "state_update"
        self.id = None
        self.post_init()
