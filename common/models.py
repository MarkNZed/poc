#!/usr/bin/env python3

"""This module has ORM entity classes"""
from datetime import datetime
from sqlalchemy import Column, Integer, String, Text, DateTime, ForeignKey, UniqueConstraint, Boolean, PickleType
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.mutable import Mutable

BASE = declarative_base()

class ChessGame(BASE):
    """This table stores games with moves and other info.
    created_on is added automatically, date is optional (if different from storing to DB time)
    """

    __tablename__ = 'chess_games'

    id = Column(Integer, primary_key=True)
    created_on = Column(DateTime, nullable=False, default=datetime.utcnow)
    event = Column(String(64))
    site = Column(String(64))
    date = Column(String(32))
    white = Column(String(64), default="Sunfish")
    black = Column(String(64), default="Sunfish")
    # Since round is a built-in python function, we will use match_round
    match_round = Column(Integer)
    # TODO Result: the result of the game. This can only have four possible values:
    # 1-0 (White won), 0-1 (Black won), 1/2-1/2 (Draw), or * (other, e.g., the game is ongoing).
    result = Column(String(64))
    white_elo = Column(Integer)
    black_elo = Column(Integer)
    eco = Column(String(32))
    moves = Column(Text())
    annotation = Column(Text())

class ChessGameMove(BASE):
    """This table stores game moves. FEN (after the move) and SAN of the move are mandatory."""

    __tablename__ = 'chess_game_moves'

    id = Column(Integer, primary_key=True)
    created_on = Column(DateTime, nullable=False, default=datetime.utcnow)
    game_id = Column(Integer, ForeignKey("chess_games.id"), nullable=False)
    # 1-based, odd turn numbers for white, even for black
    turn = Column(Integer, nullable=False)
    fen = Column(String(96), index=True, nullable=False)
    san = Column(String(16), index=True, nullable=False)
    comment = Column(Text())
    __table_args__ = (UniqueConstraint('game_id', 'turn', name='_game_turn_uc'),
                     )

class MutableDict(Mutable, dict):

    @classmethod
    def coerce(cls, key, value):
        if not isinstance(value, MutableDict):
            if isinstance(value, dict):
                return MutableDict(value)
            return Mutable.coerce(key, value)
        else:
            return value

    def __delitem(self, key):
        dict.__delitem__(self, key)
        self.changed()

    def __setitem__(self, key, value):
        dict.__setitem__(self, key, value)
        self.changed()

    def __getstate__(self):
        return dict(self)

    def __setstate__(self, state):
        self.update(self)

class MutableList(Mutable, list):
    @classmethod
    def coerce(cls, key, value):
        if not isinstance(value, MutableList):
            if isinstance(value, list):
                return MutableList(value)
            value = Mutable.coerce(key, value)
        return value
  
    def __setitem__(self, key, value):
        old_value = list.__getitem__(self, key)
        for obj, key in self._parents.items():
            old_value._parents.pop(obj, None)
  
        list.__setitem__(self, key, value)
        for obj, key in self._parents.items():
            value._parents[obj] = key
  
        self.changed()
  
    def append(self, item):
        list.append(self, item)
        self.changed()
  
    def extend(self, iterable):
        list.extend(self, iterable)
        self.changed()
  
    def insert(self, index, item):
        list.insert(self, index, item)
        self.changed()
  
    def remove(self, value):
        list.remove(self, value)
        self.changed()
  
    def reverse(self):
        list.reverse(self)
        self.changed()
  
    def pop(self, index=-1):
        item = list.pop(self, index)
        self.changed()
        return item
  
    def sort(self, cmp=None, key=None, reverse=False):
        list.sort(self, cmp, key, reverse)
        self.changed()
  
    def __getstate__(self):
        return list(self)
  
    def __setstate__(self, state):
        self[:] = state


class State(BASE):
    """This table stores current state."""

    __tablename__ = 'state'

    id = Column(Integer, primary_key=True)
    created_on = Column(DateTime, nullable=False, default=datetime.utcnow)
    name = Column(String(1024), index=True, default=None)
    attr = Column(MutableDict.as_mutable(PickleType))
    jobs = Column(MutableList.as_mutable(PickleType))
    restart = False

    def matching(self, other):
        if type(other) is type(self):
            if self.id == other.id and \
                self.created_on == other.created_on and \
                self.name == other.name:
                for key in self.attr:
                    if key not in other.attr:
                        return False
                    if self.attr[key] != other.attr[key]:
                        return False
                return True
        return False

    def __repr__(self):
        from pprint import pformat
        return pformat(vars(self), indent=4, width=1)


