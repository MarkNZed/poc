#!/usr/bin/env python3

"""Misc functions shared by different modules"""
import logging
import os
import sys
import errno
import psutil
from pprint import pformat


from common.config import datadir, logdir
from common.models import ChessGameMove

def data_path(file_name):
    """ combines file name with /data path for pickled objects saved to files """
    return os.path.join(datadir, file_name)

def log_path(file_name):
    """ combines file name with /logs path for log files """
    return os.path.join(logdir, file_name)

def ensure_dir_exist(dirname):
    """ creates directory if not exists """
    if not os.path.exists(dirname):
        try:
            os.makedirs(dirname)
        except OSError as error:
            if error.errno != errno.EEXIST:
                raise

def numeric_log_level(loglevel):
    """ parses log level passed in program args """
    numeric_level = getattr(logging, loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % loglevel)
    return numeric_level

CURRENT_FUNCNAME = lambda n=0: sys._getframe(n + 1).f_code.co_name

def restart_program():
    """Restarts the current program, with file objects and descriptors
       cleanup
    """
    try:
        proc = psutil.Process(os.getpid())
        for handler in proc.get_open_files() + proc.connections():
            os.close(handler.fd)
    except Exception as exc:
        logging.error(exc)

    python = sys.executable
    os.execl(python, python, *sys.argv)

