#!/usr/bin/env python3

"""The most common constants and classes.
Part of this configuration will be moved to YAML one day."""
import os
import redis
import logging
from sqlalchemy import create_engine

datadir = "data"
logdir = "logs"
LOG_FILE_STATE = "state.log"
LOG_FILE_MESSAGE = "message.log"
LOG_FILE_WEB = "web.log"
redis_host = os.getenv('REDIS_URL', 'redis://localhost:6379')
redis_rq_url = redis_host + '/0'
redis_messaging_url = redis_host + '/1'

redis_rq = redis.from_url(redis_rq_url)
redis_messaging = redis.from_url(redis_messaging_url)
# progress_channel = 'progress'
channel_ui = 'ui'
channel_work = 'work'
channel_state = 'state'
channel_progress = 'progress'

POSTGRES_HOST = os.getenv('POSTGRES_HOST', 'localhost')
POSTGRES_USER = os.getenv('POSTGRES_USER', 'postgres')
POSTGRES_PASS = os.getenv('POSTGRES_PASS', 'poc2017')
POSTGRES_SERVER = POSTGRES_USER+':'+POSTGRES_PASS+'@'+POSTGRES_HOST

ISOLATION_LEVEL_AUTOCOMMIT     = 0
ISOLATION_LEVEL_READ_COMMITTED = 1
ISOLATION_LEVEL_SERIALIZABLE   = 2

ENGINE = create_engine('postgresql+psycopg2://'+POSTGRES_SERVER+'/poc')

MAX_TURNS = 200

POLLING_JOBS_SECS = 10

class FlaskBase(object):
	DEBUG = False
	TESTING = False
	SQLALCHEMY_DATABASE_URI = 'postgresql://'+POSTGRES_SERVER+'/poc'

class FlaskDevelopment(FlaskBase):
    DEBUG = True
    TESTING = True
    REDIS_URL = os.getenv('REDIS_URL', 'redis://localhost:6379')
    REDIS_PASSWORD = None
    REDIS_DB = 0
    RQ_POLL_INTERVAL = 2500  #: Web interface poll period for updates in ms
