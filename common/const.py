from enum import Enum

from common import config

MAX_MOVE_MSG = 'Exceeded maximum number of turns (%d)' % config.MAX_TURNS

## All states in the machine
STATE_WAIT2START = 'wait2start'
STATE_INIT_PLAY = 'init_play'
STATE_MOVE = 'move'
STATE_WAIT4MOVE = 'wait4move'
STATE_PAUSED = 'paused'
STATE_RESULT = 'result'

class Result(Enum):
	MAX_TURNS = 1
	ABANDON = 2
	
	DRAW_3 = 3
	DRAW_50 = 4
	STALEMATE = 5
	
	WHITE_WON = 6
	BLACK_WON = 7


RESULT_AND_ANNOTATION = {
	Result.MAX_TURNS: ('*', MAX_MOVE_MSG),
	Result.ABANDON: ('*', 'abandoned'),
	Result.DRAW_50: ('1/2-1/2', 'draw by 50 moves'),
	Result.STALEMATE: ('1/2-1/2', 'stalemate'),
	Result.BLACK_WON: ('0-1', 'black won - checkmate'),
	Result.WHITE_WON: ('1-0', 'white won - checkmate')
}
