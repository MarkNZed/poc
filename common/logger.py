import logging
from common import config, utils


def setup_logger(file, level, name):
	global logger
	logging.basicConfig(filename=utils.log_path(file))
	logger = logging.getLogger(name)
	logger.setLevel(level)
	return logger

def get_logger():
	global logger
	return logger

def info(*args):
	global logger
	logger.info(*args)