#!/usr/bin/env python3

import re
import pickle
from pathlib import Path
from common import config, utils, models
from common.const import Result
from api import message, db
from worker.sunfish import Position, initial, Searcher, print_pos, MATE_LOWER, MATE_UPPER
from worker.tofen import renderFEN, WHITE, BLACK, renderSAN
from rq import get_current_job

BOARD_FILENAME = utils.data_path("board.p")
CAPTURE_OR_PAWN_ADVANCE = re.compile(r'x|^[a-h][1-8]')

def publish_position(transaction_id, game_id, side, san, hclock, db_move_id):
    """Sends message that contains endgame result (if any) to state server, or just notifies it about new move"""
    # content = message.Content('move', "%d %d %s %s" % (game_id, turn_count, san, fen))
    result = None
    if not san:
        # Previous move resulted in stalemate
        result = Result.STALEMATE
    elif san[-1:] == '#':
        # BLACK or WHITE wins
        result = Result.BLACK_WON if side else Result.WHITE_WON
    elif hclock >= 50:
        result = Result.DRAW_50
    job = get_current_job()
    data = {'job_id': job.id, 'game_id': game_id, 'db_move_id': db_move_id, 'result': result}
    if transaction_id:
        data['transaction_id'] = transaction_id
    content = message.Content('move', data)
    message.send(content, config.channel_work) 

def init_board():
    """Saves rotated initial board into pickle file"""
    utils.ensure_dir_exist(config.datadir)
    # Sunfish assumes it plays as Black, so let's feed first move to it:
    pos = Position(initial, 0, (True, True), (True, True), 0, 0, 0, 0)
    pos = pos.rotate()
    with open(BOARD_FILENAME, "wb") as boardp:
        pickle.dump(pos, boardp)
    job = get_current_job()
    content = message.Content('initialize', {'job_id': job.id})
    message.send(content, config.channel_work)

def load_search(turn):
    #if turn > 1:
    #    file = Path(utils.data_path("search_"+str(turn - 1)+".p"))
    #    if file.is_file():
    #        with file.open("rb") as searcher:
    #           return pickle.load(searcher)
    return Searcher()

def save_search(searcher, turn):
    pass
    #file = utils.data_path("search_"+str(turn - 1)+".p")
    #with open(file, "wb+") as fh:
    #    pickle.dump(searcher, fh)

def play_white(game_id, turn):
    play_side(game_id, WHITE, 1, turn)

def play_black(game_id, turn):
    play_side(game_id, BLACK, 0.2, turn)

def play_side(game_id, side, time, turn):
    """Plays as white or black, with specified time to search the tree"""
    # player = ["White", "Black"][side]
    with open(BOARD_FILENAME, "rb") as boardp:
        pos = pickle.load(boardp)

    searcher = load_search(turn)
    move, score = searcher.search(pos, secs=time)
    # Stalemate if there's no move found for the current player. Stalemate is Draw
    if move is None:
        return publish_position(None, game_id, side, None, pos.hclock, None)

    san = renderSAN(pos, move)
    hclock = 0 if CAPTURE_OR_PAWN_ADVANCE.search(san) else pos.hclock + 1

    pos = Position(pos.board, pos.score, pos.wc, pos.bc, \
        pos.ep, pos.kp, pos.turn, hclock).move(move)
    fen = renderFEN(pos, pos.hclock, (turn - 1) // 2 + 1 + side)

    with open(BOARD_FILENAME, "wb") as boardp:
        pickle.dump(pos, boardp)

    save_search(searcher, turn)

    dbmove = models.ChessGameMove(game_id=game_id, turn=turn, fen=fen, san=san)
    db.add(dbmove)
    db.prepare()

    # publish position only after adding the move, so that state.py will correctly finalise
    # if pos.score <= -MATE_LOWER:
        # publish_result(game_id, 'win')

    publish_position(db.transaction_id(), game_id, side, san, hclock, dbmove.id)

    return {'transaction_id':db.transaction_id()}
