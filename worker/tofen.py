#!/usr/bin/env python3

import itertools
import re
from worker import sunfish
"""
Utilities to convert sunfish.Position to FEN and to write PGN for the game
Taken from https://github.com/thomasahle/sunfish/blob/master/tools.py
"""
# find_piece = re.compile(r'[\.rnbqkp]', re.IGNORECASE)

WHITE, BLACK = range(2)

def renderFEN(pos, half_move_clock=0, full_move_clock=1):
    color = 'wb'[get_color(pos)]
    if get_color(pos) == BLACK:
        pos = pos.rotate()
    board = '/'.join(pos.board.split())
    board = re.sub(r'\.+', (lambda m: str(len(m.group(0)))), board)
    castling = ''.join(itertools.compress('KQkq', pos.wc[::-1]+pos.bc)) or '-'
    ep = sunfish.render(pos.ep) if not pos.board[pos.ep].isspace() else '-'
    clock = '{} {}'.format(half_move_clock, full_move_clock)
    return ' '.join((board, color, castling, ep, clock))
def get_color(pos):
    ''' A slightly hacky way to to get the color from a sunfish position '''
    return WHITE if pos.board.startswith('\n') else BLACK

def gen_legal_moves(pos):
    ''' pos.gen_moves(), but without those that leaves us in check.
        Also the position after moving is included. '''
    for move in pos.gen_moves():
        pos1 = pos.move(move)
        # If we just checked for opponent moves capturing the king, we would miss
        # captures in case of illegal castling.
        if not any(pos1.value(m) >= sunfish.MATE_LOWER for m in pos1.gen_moves()):
            yield move, pos1

def mrender(pos, m):
    # Sunfish always assumes promotion to queen
    p = 'q' if sunfish.A8 <= m[1] <= sunfish.H8 and pos.board[m[0]] == 'P' else ''
    m = m if get_color(pos) == WHITE else (119-m[0], 119-m[1])
    return sunfish.render(m[0]) + sunfish.render(m[1]) + p

def mparse(color, move):
    m = (sunfish.parse(move[0:2]), sunfish.parse(move[2:4]))
    return m if color == WHITE else (119-m[0], 119-m[1])
'''
1. d4 d5 2. c4 dxc4 3. Nf3 b5 4. e4 Bd7 5. d5 e5 6. Nbd2 Bd6 7. Be2 Nf6 8. O-O O-O 9. h3 c6 10. dxc6 Bxc6 11. g4 a6 12. Qc2 Nbd7 13. g5 Ne8 14. h4 g6 15. Kh1 Qc8 16. Rg1 Nc5 17. b3 Qh3+ 18. Nh2 Qxh4 19. bxc4 Nxe4 20. Nxe4 Bxe4+ 21. Rg2 Bxc2 0-1
1. d4 d5 2. c4 dxc4 3. e4 Nc6 4. Bxc4 Qxd4 5. Qxd4 Nxd4 6. Be3 Nc2+ 7. Ke2 Nxa1 8. Na3 Bg4+ 9. f3 Bh5 10. Nh3 e6 11. Rxa1 Bxa3 12. bxa3 Ne7 13. Rd1 O-O 14. g4 Bg6 15. Nf4 h6 16. h4 a6 17. h5 Bh7 18. Nd3 Rfd8 19. Nc5 Rxd1 20. Kxd1 b6 21. Nxa6 Rd8+ 22. Ke2 Nc6 23. Nxc7 Nd4+ 24. Bxd4 Rxd4 25. Bb3 Rd7 26. Nb5 Rc7 27. a4 Rc5 28. Nd6 f5 29. Nc4 Kf7 30. Nxb6 fxe4 31. f4 Kf6 32. Nd7+ Ke7 33. Nxc5 Kd6 34. Nb7+ Kc6 35. Na5+ Kb6 36. Nc4+ Kc5 37. a5 Bg8 38. Ke3 e5 39. Kxe4 Bxc4 40. Bxc4 Kxc4 41. a6 Kb5 42. a7 Kb6 43. a8=Q Kc5 44. Qf8+ Kc4 45. Qxg7 exf4 46. Kxf4 Kb4 47. Qxh6 Ka3 48. Qd6+ Kxa2 49. h6 1-0
'''
def renderSAN(pos, move):
    ''' Assumes board is rotated to position of current player '''
    i, j = move
    csrc, cdst = sunfish.render(i), sunfish.render(j)
    # Rotate for black
    if get_color(pos) == BLACK:
        csrc, cdst = sunfish.render(119-i), sunfish.render(119-j)
    # Check
    pos1 = pos.move(move)
    cankill = lambda p: any(p.board[b]=='k' for a,b in p.gen_moves())
    check = ''
    if cankill(pos1.rotate()):
        check = '+'
        if all(cankill(pos1.move(move1)) for move1 in pos1.gen_moves()):
            check = '#'
    # Castling
    if pos.board[i] == 'K' and abs(i-j) == 2:
        if get_color(pos) == WHITE and j > i or get_color(pos) == BLACK and j < i:
            return 'O-O' + check
        else:
            return 'O-O-O' + check
    # Pawn moves
    if pos.board[i] == 'P':
        pro = '=Q' if sunfish.A8 <= j <= sunfish.H8 else ''
        cap = csrc[0] + 'x' if pos.board[j] != '.' or j == pos.ep else ''
        return cap + cdst + pro + check
    # Figure out what files and ranks we need to include
    srcs = [a for (a,b),_ in gen_legal_moves(pos) if pos.board[a] == pos.board[i] and b == j]
    srcs_file = [a for a in srcs if (a - sunfish.A1) % 10 == (i - sunfish.A1) % 10]
    srcs_rank = [a for a in srcs if (a - sunfish.A1) // 10 == (i - sunfish.A1) // 10]
    assert srcs, 'No moves compatible with {}'.format(move)
    if len(srcs) == 1: src = ''
    elif len(srcs_file) == 1: src = csrc[0]
    elif len(srcs_rank) == 1: src = csrc[1]
    else: src = csrc
    # Normal moves
    p = pos.board[i]
    cap = 'x' if pos.board[j] != '.' else ''
    return p + src + cap + cdst + check
