#!/usr/bin/env python3

"""Web application main file"""

import threading
import logging
import argparse
import os
import functools

from flask import Flask, Response, request, send_from_directory
from flask_sqlalchemy import SQLAlchemy
import rq_dashboard

from common import config, utils
from api import message, ui
from webapp import sse
# from common.models import State

# Flush buffer to keep logs up to date
print = functools.partial(print, flush=True)

DEF_HOST = "0.0.0.0"
DEF_PORT = 5003
app = Flask(__name__, static_folder='../webapp/static')
app.config.from_object(config.FlaskDevelopment)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
logger = logging.getLogger(__name__)

logger_sql = logging.getLogger('sqlalchemy.engine')
logger_sql.setLevel(logging.INFO)


@app.route('/assets/<path:path>')
def send_js(path):
    """Web assets: js, css, png etc. handling"""
    return send_from_directory(app.static_folder, path)

@app.route('/')
def root():
    return app.send_static_file('index.html')

def event_stream():
    """Generator for server-sent events"""
    for msg in message.listen(config.channel_state):
        content = msg.content
        logger.debug(msg.content)
        evt0 = sse.GameBoardEvent(content, db)
        if evt0.event != None:
            yield evt0.encode()
        evt1 = sse.StateServerEvent(content)
        if evt1.event != None:
            yield evt1.encode()

@app.route('/api/v1/stream')
def stream():
    """Endpoint for server-sent events"""
    return Response(event_stream(), mimetype="text/event-stream")

@app.route("/rest/v1/ctl", methods=["POST"])
def control_state_serv():
    """Controls on the page submit to this endpoint"""
    command = request.form['spec']
    logger.debug(command)
    if command == 'start':
        ui.start()
    elif command == 'stop':
        ui.stop()
    elif command in ['pause', 'resume']:
        ui.toggle_pause()
    else:
        message_warn = "Unknown command: " + command
        logger.warning(message_warn)
        return message_warn, 404
    return request.form['spec'], 200

@app.route("/restart")
def restart():
    utils.restart_program()
    #return Response("OK Restarting", mimetype="text/plain")
    # We will never return a response because of the restart
    return None

@app.route("/shutdown")
def shutdown():
    ui.shutdown()
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()


##################################################################

def ui_monitor():
    """Thread job to listen to shutdown and restart UI commands"""
    for msg in message.listen(config.channel_ui):
        if msg.content.cmd == 'shutdown':
            # hack to avoid Flask catch the sys.exit() Exception
            os._exit(0)
        elif msg.content.cmd == 'restart':
            utils.restart_program()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="PoC Web Server based on Flask", prog="Web Server")
    parser.add_argument('-d', '--debug', \
        action='store_true', help='use debug mode (default: False)')
    parser.add_argument('-r', '--reloader', \
        action='store_true', help='use reloader (default: False)')
    parser.add_argument('-p', '--port', \
        type=int, default=DEF_PORT, help='bind to port # (default: %(DEF_PORT)i)' % globals())
    parser.add_argument('-l', '--log', \
        choices=('warn', 'info', 'debug'), default='info', help='set log level (default: INFO)')
    parser.add_argument('-f', '--logfile', \
        type=str, default=config.LOG_FILE_WEB, \
        help='log filename for logging into logs/ (default: %s)' % config.LOG_FILE_WEB)
    parser.add_argument('host', type=str, default=DEF_HOST, \
        nargs='?', help='the host name to run the server (default: %(DEF_HOST)s)' % globals())
    utils.ensure_dir_exist(config.logdir)
    args = parser.parse_args()

    numeric_level = utils.numeric_log_level(args.log)
    logging.basicConfig(level=numeric_level, filename=utils.log_path(args.logfile))

    # For RQ Dashboard
    app.register_blueprint(rq_dashboard.blueprint, url_prefix="/rq")

    messageThread = threading.Thread(target=ui_monitor)
    messageThread.start()

    # threaded is essential for SSE, so not parameterised!
    app.run(debug=args.debug, host=args.host, port=args.port, \
        use_reloader=args.reloader, threaded=True)
