#!/usr/bin/env python3

import os

from sqlalchemy import create_engine
from common import config
from web import db

# Connect to postgres from CLI
#psql -h $POSTGRES_HOST -U postgres poc

if __name__ == '__main__':
	# http://stackoverflow.com/questions/6506578/how-to-create-a-new-database-using-sqlalchemy
	ENGINE = create_engine('postgresql://'+config.POSTGRES_SERVER)
	conn = ENGINE.connect()
	conn.execute("commit")
	conn.execute("create database poc")
	conn.close()

	db.create_all()
