#!/usr/bin/env python3

import os

from sqlalchemy import create_engine
from common import config

# Connect to postgres from CLI
#psql -h $POSTGRES_HOST -U postgres poc

def cleanup_prepared():
    ISOLATION_LEVEL_AUTOCOMMIT     = 0
    ISOLATION_LEVEL_READ_COMMITTED = 1
    ISOLATION_LEVEL_SERIALIZABLE   = 2
    conn = config.ENGINE.connect()
    conn.connection.connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    trs = [t[0] for t in conn.execute('select gid from pg_prepared_xacts')]
    #conn.execute("rollback;")
    for t in trs:
        # Could not use conn.rollback_prepared because it starts a transaction
        conn.execute("ROLLBACK PREPARED '"+t+"'")
    conn.connection.connection.set_isolation_level(ISOLATION_LEVEL_READ_COMMITTED)
    conn.close()
    config.ENGINE.dispose()

if __name__ == '__main__':
	cleanup_prepared()
	# http://stackoverflow.com/questions/6506578/how-to-create-a-new-database-using-sqlalchemy
	ENGINE = create_engine('postgresql://'+config.POSTGRES_SERVER)
	conn = ENGINE.connect()
	conn.execute("commit")
	conn.execute("drop database poc")
	conn.close()
