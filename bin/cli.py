#!/usr/bin/env python3

import click, sys
from common import config, utils
from api import message, ui
# ipython config
from traitlets.config.loader import Config
from IPython.terminal.prompts import Prompts, Token
# First import the embeddable shell class
from IPython.terminal.embed import InteractiveShellEmbed
import functools
# Flush buffer to keep logs up to date
print = functools.partial(print, flush=True)

class CustomPrompt(Prompts):

    def in_prompt_tokens(self, cli=None):
        return [
            (Token.Prompt, 'In <'),
            (Token.PromptNum, str(self.shell.execution_count)),
            (Token.Prompt, '>: '),
            ]

    def out_prompt_tokens(self):
        return [
            (Token.OutPrompt, 'Out<'),
            (Token.OutPromptNum, str(self.shell.execution_count)),
            (Token.OutPrompt, '>: '),
        ]

cfg = Config()
cfg.TerminalInteractiveShell.prompts_class = CustomPrompt

# Now create an instance of the embeddable shell. The first argument is a
# string with options exactly as you would type them if you were starting
# IPython at the system command line. Any parameters you want to define for
# configuration can thus be specified here.
ipshell = InteractiveShellEmbed(
    config=cfg,
    banner1='Welcome to PoC.',
    exit_msg='Leaving PoC.')

def start():
    ui.start()

def stop():
    ui.stop()

def shutdown():
    ui.shutdown()
    s = get_ipython()
    s.exiter()
 
def restart():
    ui.restart()
    utils.restart_program()

def get_state():
    return ui.get_state()

 
##############################################################################
### The following section is related to click for options etc
##############################################################################
@click.command()
@click.option('--count', default=1, help='Number of greetings.')

def hello(count):
    """Simple program that starts PoC."""
    for x in range(count):
        click.echo('Hello!')
    ipshell('Hit Ctrl-D to exit.')

##############################################################################

if __name__ == '__main__':
    hello()
