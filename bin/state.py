#!/usr/bin/env python3

"""
Initial state is wait2start;
Commands arrive from Flask web UI or CLI UI over 'ui' channel;
Upon receiving 'start' command, create chess_games record in the DB;
Post task for init_board and wait for m.content.cmd == 'initialize' in state=play_white;
Upon receiving play_white answer over 'game' channel, post play_black;
Upon receiving m.content.cmd == 'outcome' over 'game' channel,
    print it and save to chess_games.result;
Upon exceeding max_turns, end game, change state to 'wait2start' and save chess_games.result;
Worker will append new FEN/SAN to chess_game_moves table in DB;
Notify 'state' channel about state transitions;
Listen for 'shutdown'/'restart' commands on channel 'ui';
UI can request.name state by sending m.content.cmd == 'get_state'
    and get m.id and state back in response.
"""

import datetime
import sys
import logging
import functools
import copy
import time
from sqlalchemy.orm import sessionmaker
from common.models import BASE, ChessGame, State
from common import config, utils, const
from api import message, queue, ui, db
from pprint import pformat, pprint


# Flush buffer to keep logs up to date
print = functools.partial(print, flush=True)

logging.basicConfig(filename=utils.log_path(config.LOG_FILE_STATE))

logger_sql = logging.getLogger('sqlalchemy.engine')
#logger_sql.setLevel(logging.INFO)
logger_sql.setLevel(logging.DEBUG)


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

BASE.metadata.create_all(config.ENGINE, checkfirst=True)
#Session = sessionmaker(bind=config.ENGINE, autocommit=True)
Session = sessionmaker(bind=config.ENGINE)
session = Session()

two_phase_conn = config.ENGINE.connect()
two_phase_conn.connection.connection.set_isolation_level(config.ISOLATION_LEVEL_AUTOCOMMIT)

#### State Machine Infrastructure ####

def init_machine():
    global current_state, next_state, last_poll_seconds
    # Discard all outstanding jobs
    db.cleanup_prepared()
    queue.delete_failed_jobs()
    # Get the previous state from DB
    next_state = session.query(State).get(1)
    if next_state == None:
        next_state = State(name=initial_state, attr=initial_attr, jobs=[])
    else:
        next_state.restart = True
        next_state.jobs = []
        content = message.Content('restart', next_state.name)
        message.send(content, config.channel_state)
    current_state = copy.deepcopy(next_state)
    session.add(next_state)
    last_poll_seconds = int(time.time() * 1000 * 1000)
   

def filter_jobs(m):
    global current_state, next_state
    # If we don't know about this job then skip the message
    if m.channel == config.channel_work:
        if isinstance(m.content.data, dict) and 'job_id' in m.content.data.keys():
            if not m.content.data['job_id'] in current_state.jobs:
                # If it includes a transaction then rollback the transaction
                if isinstance(m.content.data, dict) and 'transaction_id' in m.content.data.keys():
                    try:
                        two_phase_conn.execute('ROLLBACK PREPARED '+"'"+m.content.data['transaction_id']+"'")
                    except Exception as e:
                        logger.warning("EXCEPTION:",e)
                logger.warning("Skipping message from unknown job", m.content.data['job_id'])
                return False
    return True

def framework_actions(m):
    global current_state, next_state
    if m.channel == config.channel_ui:
        if m.content.cmd == 'shutdown':
            sys.exit()
        elif m.content.cmd == 'restart':
            utils.restart_program()
        elif m.content.cmd == 'get_state':
            data = {'id': m.id, 'state': current_state}
            content = message.Content('response', data)
            message_id = message.send(content, config.channel_state)
    return True

def begin_cycle(m):
    global current_state, next_state
    if m.channel == config.channel_state and m.content.cmd == 'init':
        init_machine()
    else:
        next_state.restart = False
    filter_jobs(m)
    framework_actions(m)

def end_cycle(m):
    global current_state, next_state, last_poll_seconds
    if (not current_state.matching(next_state) or next_state.restart):
        old_state_name = current_state.name
         # If we know about this job then pop it off our jobs list
        if m.channel == config.channel_work:
            if isinstance(m.content.data, dict) and 'job_id' in m.content.data.keys():
                if m.content.data['job_id'] in next_state.jobs:
                    index = next_state.jobs.index(m.content.data['job_id'])
                    next_state.jobs.pop(index)
        current_state = copy.deepcopy(next_state)
        # Should ensure that session and transaction both succeed. 
        # session could commit and two_phas_conn fail
        session.commit()
        # If the message includes a transaction_id then we complete
        # the two phase commit
        if isinstance(m.content.data, dict) and 'transaction_id' in m.content.data.keys():
            try:
                two_phase_conn.execute('COMMIT PREPARED '+"'"+m.content.data['transaction_id']+"'")
            except Exception as e:
                logger.warning("EXCEPTION:",e)
        content = message.Content('repeat' if next_state.name == old_state_name else 'transition', next_state)
        message.send(content, config.channel_state)
        current_seconds = int(time.time() * 1000 * 1000)
        if current_seconds - last_poll_seconds > config.POLLING_JOBS_SECS:
            if queue.failed_jobs_exist():
                raise ValueError('Polling found failed jobs in RQ')   

def unexpected_cmd(m, channel):
    global current_state
    found = None
    if isinstance(channel, list):
        for ch in channel:
            if m.channel == channel:
                found = channel
    else:
        if m.channel == channel:
            found = channel
    if found:
        print("State", current_state.name, "Channel", found, "Unknown cmd", m.content.cmd)

def state(name):
    global current_state
    return current_state.name == name

def next(name):
    global next_state
    next_state.name = name

#### Application specific State Machine behavior ####

# set the initial state
initial_state = const.STATE_WAIT2START
initial_attr = {'game_id': None, 'move_count': 0, 'player': 'white', 'db_move_id': None, 'game_result': None}

# finalise the DB record: populate chess_games.moves from chess_game_moves.san
# @see worker.tasks.play_side
def chess_end(state, result):
    global next_state
    game = session.query(ChessGame).get(state.attr['game_id'])
    game.result, game.annotation = const.RESULT_AND_ANNOTATION[result]
    next_state.attr['game_result'] = result

def chess_create():
    now = datetime.datetime.now().replace(microsecond=0)
    game = ChessGame(date=now.isoformat())
    session.add(game)
    # Need to commit to ensure the entry is available for the job
    session.commit() 
    return game.id

def chess_move(state, next_state):
    if state.attr['player'] == 'white':
        queue.enqueue(next_state, 'worker.tasks.play_white', next_state.attr['game_id'], next_state.attr['move_count'])
        next_state.attr['player'] = 'black'
    else:
        job = queue.enqueue(next_state, 'worker.tasks.play_black', next_state.attr['game_id'], next_state.attr['move_count'])
        next_state.attr['player'] = 'white'

#### State Machine Behavior ####
def cycle(m):
    ##########################################################################
    if state(const.STATE_WAIT2START):
        if m.cmd(config.channel_ui, 'start'):
            next_state.attr['game_id'] = chess_create()
            next_state.attr['game_result'] = None
            next_state.attr['move_count'] = 1
            next_state.attr['player'] = 'white'
            queue.enqueue(next_state, "worker.tasks.init_board")
            next_state.name = const.STATE_INIT_PLAY
            return next(const.STATE_INIT_PLAY)
    ##########################################################################
    elif state(const.STATE_INIT_PLAY):
        if m.cmd(config.channel_work, 'initialize'):
            # Make first move
            chess_move(current_state, next_state)
            return next(const.STATE_WAIT4MOVE)
        if m.cmd(config.channel_state, 'restart'):
            return next(const.STATE_WAIT2START)
    ##########################################################################
    elif state(const.STATE_MOVE):
        # We arrive in this state after a move has been made
        if m.cmd(config.channel_ui, 'stop'):
            chess_end(current_state, const.Result.ABANDON)
            return next(const.STATE_RESULT)
        if m.cmd(config.channel_ui, 'toggle_pause'):
            return next(const.STATE_PAUSED)
        next_state.attr['move_count'] += 1           
        if current_state.attr['move_count'] > config.MAX_TURNS:
            chess_end(current_state, const.Result.MAX_TURNS)
            return next(const.STATE_RESULT)
        chess_move(current_state, next_state)
        return next(const.STATE_WAIT4MOVE)
    ##########################################################################
    elif state(const.STATE_WAIT4MOVE):
        if m.cmd(config.channel_ui, 'stop'):
            chess_end(current_state, const.Result.ABANDON)
            return next(const.STATE_RESULT)
        if m.cmd(config.channel_ui, 'toggle_pause'):
            return next(const.STATE_PAUSED)
        if m.cmd(config.channel_work, 'move'):
            next_state.attr['db_move_id'] = m.content.data['db_move_id']
            if m.content.data['result']:
                chess_end(current_state, m.content.data['result'])
                return next(const.STATE_RESULT)
            else:                
                return next(const.STATE_MOVE)
        if m.cmd(config.channel_state, 'restart'):
            # Replay the last move
            next_state.attr['move_count'] -= 1
            return next(const.STATE_MOVE)
    ##########################################################################
    elif state(const.STATE_PAUSED):
        if m.cmd(config.channel_ui, 'toggle_pause'):
            return next(const.STATE_MOVE)
        if m.cmd(config.channel_work, 'move'):
            next_state.attr['db_move_id'] = m.content.data['db_move_id']
            if m.content.data['result']:
                chess_end(current_state, m.content.data['result'])
                return next(const.STATE_RESULT)
            else: # send move to UI but stay in PAUSED, waiting for toggle_pause cmd
                return next(const.STATE_PAUSED)
    ##########################################################################
    elif state(const.STATE_RESULT):
        return next(const.STATE_WAIT2START)
    ##########################################################################
    unexpected_cmd(m, [config.channel_ui, config.channel_work])


# The state machine iterates when receiving a message, except for the very first call to message.listen
# which returns an init command on the channel_state
for m in message.listen([config.channel_work, config.channel_ui, config.channel_state], config.channel_state):
    begin_cycle(m)
    cycle(m)
    end_cycle(m)

