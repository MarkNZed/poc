#!/usr/bin/env python3

import threading, os, signal, sys
import worker
from api import message

"""
Maybe this is a good test task:

Build a server combining redis for messaging in flask.
For the worker it does not need flask
"""
# http://python-rq.org/docs/workers/

# Preload libraries here so that each job doesn't do that separately
# 
from worker.sunfish import Position, initial
from worker import tofen
 
import sys
from rq import Connection, Worker
from common import config, utils
import logging, argparse
import functools
# Flush buffer to keep logs up to date
print = functools.partial(print, flush=True)

# assuming loglevel is bound to the string value obtained from the
# command line argument. Convert to upper case to allow the user to
# specify --log=DEBUG or --log=debug
utils.ensure_dir_exist(config.logdir)
parser = argparse.ArgumentParser(description='PoC Workers Runner')
parser.add_argument('queues', type=str, nargs='*', \
    help='the queue name(s) for this worker')
parser.add_argument('-l', '--log', choices=('warn', 'info', 'debug'), default='warn', \
    help='set log level (default: INFO)')
args = parser.parse_args()
numeric_level = utils.numeric_log_level(args.log)
logging.basicConfig(level=numeric_level, filename=utils.log_path('worker_%s.log' % (args.queues[0] if args.queues else 'default')))
logger = logging.getLogger(__name__)

def ui_monitor():
    for m in message.listen(config.channel_ui):
        if m.content.cmd == 'shutdown':
            # hack to avoid Flask catch the sys.exit() Exception
            print("Shutting down")
            # Raise signal to shut down cleanly see http://python-rq.org/docs/workers/
            #os.kill(os.getpid(), signal.SIGTERM)
            #sys.exit()
            os._exit(0)
        elif m.content.cmd == 'restart':
         	utils.restart_program()

messageThread = threading.Thread(target=ui_monitor)
messageThread.start()


# Provide queue names to listen to as arguments to this script,
# similar to rq worker
with Connection(config.redis_rq):
    qs = args.queues or ['default']

    w = Worker(qs)
    w.work()
