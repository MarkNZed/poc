#!/bin/bash

rm -f logs/*

./bin/state.py &> logs/run_state.log &
./bin/web.py &> logs/run_web.log &
./bin/start_worker.py &> logs/run_worker1.log &
